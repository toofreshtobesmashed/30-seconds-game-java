package sk.murin.game_30_seconds;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

public class Game implements ActionListener {

    private final JLabel labelTime;
    private final JLabel labelPoints;
    private final JButton[] buttons = new JButton[95];
    private final JButton buttonStart;

    private int seconds = 30;
    private int points = 0;
    private Timer timer;
    private boolean ableToClick = false;
    private int position = 0;

    public Game() {
        JFrame frame = new JFrame("Extrémna hra");
        frame.setSize(700, 330);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());

        JPanel panelTop = new JPanel();
        panelTop.setLayout(new GridLayout(1, 2));
        JPanel panelCenter = new JPanel();
        panelCenter.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        JPanel panelDole = new JPanel();

        labelTime = new JLabel(" ");
        labelTime.setFont(new Font("Consolas", Font.BOLD, 18));

        labelTime.setHorizontalAlignment(SwingConstants.CENTER);
        labelTime.setLocation(15, 2);
        panelTop.add(labelTime);
        labelPoints = new JLabel("Počet bodov " + points);
        labelPoints.setHorizontalAlignment(SwingConstants.LEADING);
        labelPoints.setFont(new Font("Consolas", Font.BOLD, 18));
        panelTop.add(labelPoints);

        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new JButton();
            buttons[i].setPreferredSize(new Dimension(35, 35));
            panelCenter.add(buttons[i]);
            buttons[i].addActionListener(this);
        }
        Collections.shuffle(Arrays.asList(buttons));

        buttonStart = new JButton("Štart");
        buttonStart.setPreferredSize(new Dimension(150, 50));
        buttonStart.setFocusable(false);
        buttonStart.setFont(new Font("Consolas", Font.BOLD, 20));

        for (JButton b : buttons) {
            b.setEnabled(false);
        }

        buttonStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonStart.setEnabled(false);
                ableToClick = true;
                for (JButton button : buttons) {
                    button.setEnabled(true);
                }
                timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        paintRandomBtn();
                        labelTime.setText(String.valueOf(seconds));
                        if (seconds == 0) {
                            ableToClick = false;
                            timer.cancel();
                            for (JButton button : buttons) {
                                button.setEnabled(false);
                            }
                            buttonStart.setEnabled(false);
                            JOptionPane.showMessageDialog(null, "Stihol si kliknúť na " + points + " políčok", "Si štastný?", JOptionPane.INFORMATION_MESSAGE);
                        }
                        seconds--;
                    }
                }, 0, 1000);
            }
        });
        panelDole.add(buttonStart);

        frame.add(panelTop, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelDole, BorderLayout.SOUTH);
        frame.setResizable(false);
        frame.setVisible(true);
    }


    public void actionPerformed(ActionEvent e) {
        JButton buttonPressed = (JButton) e.getSource();
        buttonPressed.setBackground(Color.ORANGE);

        if (ableToClick) {
            if (buttons[position - 1] == buttonPressed) {
                points++;
            }
            labelPoints.setText("Počet bodov " + points);

        }
    }

    public void paintRandomBtn() {
        buttons[position].setBackground(Color.blue);
        position++;
        System.out.println(points + " bodov");
    }
}
